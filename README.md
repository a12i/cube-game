# Cube (game demo)

Levitate the cube by pointing the hand controller towards it and holding any of the grab handles. During levitation, the cube reacts to the hand controller positions and velocity, and will amplify the last impulse upon the grab handle's release. The goal of the game is to hit all vertices of the cube with a sufficient impact velocity. Each vertex lights up if the hit was sufficient. When the goal is accomplished, you'll see green lights and the cube grows. The game round then starts again.

## Gameplay video

[![gameplay video](http://img.youtube.com/vi/oQ9mQ_sH1FM/0.jpg)](http://www.youtube.com/watch?v=oQ9mQ_sH1FM "gameplay video")

https://youtu.be/oQ9mQ_sH1FM

## Target platform

Oculus Quest. Uses Unity's new [XR Interaction Toolkit](https://docs.unity3d.com/Packages/com.unity.xr.interaction.toolkit@0.9/manual/index.html) package, so in theory it might work on other XR devices as well.

## Installation

An .apk for the Oculus Quest is available to [sideload](https://uploadvr.com/how-to-sideload-apps-oculus-go/) at the following link:

https://www.dropbox.com/s/savmslznt5pvn9g/a12i_cube-game.apk?dl=0

## Notes

Most of the game logic is in [`Cube.cs`](Assets/Cube.cs) and [`VertexCollider.cs`](Assets/VertexCollider.cs):
