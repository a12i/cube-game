﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VertexCollider : MonoBehaviour {
    public AudioClip audioClip;
    public AudioSource audioSource;
    public float impactMagnitudeSensitivity = 8f;
    public float impactMagnitudeMinimum = 2f;
    public float rangeGrowthMagnitude = 0.5f;
    public bool hasBeenHit = false;

    private Vector3 lastPosition;
    private Vector3 currentVelocity;

    void FixedUpdate() {
        // calculate velocity
        var currentPosition = gameObject.transform.position;
        var deltaPosition = (currentPosition - lastPosition);
        currentVelocity = deltaPosition / Time.fixedDeltaTime;
        lastPosition = currentPosition;
    }

    private void OnTriggerEnter(Collider other) {
        // the goal of the game is to hit all 8 vertices, then the cube changes
        if (other.gameObject.name == "Plane" && currentVelocity.magnitude > impactMagnitudeSensitivity) {
            GetComponent<Light>().range += rangeGrowthMagnitude;
            hasBeenHit = true;
            audioSource.PlayOneShot(audioClip, 0.1f * currentVelocity.magnitude);
        } else if (other.gameObject.name == "Plane" && currentVelocity.magnitude > impactMagnitudeMinimum) {
            // still play a sound but no hit
            audioSource.PlayOneShot(audioClip, 0.1f * currentVelocity.magnitude);
        }
    }
}
