﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

public class Cube : MonoBehaviour {
    #region exposed variables
    public bool isLevitating = false;
    public float forceMultipler = 1f;
    public float levitationCeiling = 3f;
    public GameObject leftController;
    public GameObject rightController;
    public float velocityUpdateFactor = 0.1f;
    public float angularVelocityUpdateFactor = 10f;
    public float velocityReleaseFactor = 100f;
    public float angularVelocityReleaseFactor = 25f;
    public Vector3 targetScale = new Vector3(1f, 1f, 1f);
    public float growthFactor = 1.5f;
    public AudioClip growAudioClip;
    public AudioSource audioSource;
    #endregion

    #region internals + velocities
    private XRSimpleInteractable simpleInteractable = null;
    private Rigidbody rb;

    private Vector3 lastLeftPosition;
    private Vector3 leftHandVelocity;
    private Vector3 leftHandAngularVelocity;
    private UnityEngine.Quaternion lastLeftRotation;

    private Vector3 lastRightPosition;
    private Vector3 rightHandVelocity;
    private Vector3 rightHandAngularVelocity;
    private UnityEngine.Quaternion lastRightRotation;
    #endregion

    #region game logic
    public float lightRangeTreshold = 0.5f;
    private GameObject[] cubeVerticesWithLight;
    private bool isGrowing = false;
    #endregion

    private void Awake() {
        simpleInteractable = GetComponent<XRSimpleInteractable>();
        simpleInteractable.onSelectEnter.AddListener(StartLevitating);
        simpleInteractable.onSelectExit.AddListener(StopLevitating);
    }

    private void OnDestroy() {
        simpleInteractable.onSelectEnter.RemoveListener(StartLevitating);
        simpleInteractable.onSelectExit.RemoveListener(StopLevitating);
    }

    private void StartLevitating(XRBaseInteractor interactor)
    {
        isLevitating = true;
    }

    private void StopLevitating(XRBaseInteractor interactor)
    {
        isLevitating = false;
        Vector3 midPointVelocity = Vector3.Lerp(leftHandVelocity, rightHandVelocity, 0.5f);
        Vector3 midPointAngularVelocity = Vector3.Lerp(leftHandAngularVelocity, rightHandAngularVelocity, 0.5f);
        rb.AddForce(midPointVelocity * velocityReleaseFactor);
        rb.AddTorque(midPointAngularVelocity * angularVelocityReleaseFactor);
    }

    // Start is called before the first frame update
    void Start() {
        lastLeftPosition = leftController.transform.position;
        lastLeftRotation = leftController.transform.rotation;
        lastRightPosition = rightController.transform.position;
        lastRightRotation = rightController.transform.rotation;

        rb = GetComponent<Rigidbody>();

        cubeVerticesWithLight = GameObject.FindGameObjectsWithTag("Vertices");
    }

    // Frame-rate independent update loop for physics calculations.
    void FixedUpdate() {
        // calculate velocities

        var currentLeftPosition = leftController.transform.position;
        var deltaLeft = (currentLeftPosition - lastLeftPosition);
        leftHandVelocity = deltaLeft / Time.fixedDeltaTime;
        lastLeftPosition = currentLeftPosition;

        var currentRightPosition = rightController.transform.position;
        var deltaRight = (currentRightPosition - lastRightPosition);
        rightHandVelocity = deltaRight / Time.fixedDeltaTime;
        lastRightPosition = currentRightPosition;

        // calculate angular velocities

        var currentLeftRotation = leftController.transform.rotation;
        Quaternion deltaLeftRotation = currentLeftRotation * Quaternion.Inverse(lastLeftRotation);
        Vector3 eulerLeftRotation = new Vector3(
            Mathf.DeltaAngle(0, Mathf.Round(deltaLeftRotation.eulerAngles.x)),
            Mathf.DeltaAngle(0, Mathf.Round(deltaLeftRotation.eulerAngles.y)),
            Mathf.DeltaAngle(0, Mathf.Round(deltaLeftRotation.eulerAngles.z)));

        leftHandAngularVelocity = eulerLeftRotation / Time.fixedDeltaTime * Mathf.Deg2Rad;
        lastLeftRotation = currentLeftRotation;

        var currentRightRotation = rightController.transform.rotation;
        Quaternion deltaRightRotation = currentRightRotation * Quaternion.Inverse(lastRightRotation);
        Vector3 eulerRightRotation = new Vector3(
            Mathf.DeltaAngle(0, Mathf.Round(deltaRightRotation.eulerAngles.x)),
            Mathf.DeltaAngle(0, Mathf.Round(deltaRightRotation.eulerAngles.y)),
            Mathf.DeltaAngle(0, Mathf.Round(deltaRightRotation.eulerAngles.z)));

        rightHandAngularVelocity = eulerRightRotation / Time.fixedDeltaTime * Mathf.Deg2Rad;
        lastRightRotation = currentRightRotation;

        // update forces

        Vector3 midPointVelocity = Vector3.Lerp(leftHandVelocity, rightHandVelocity, 0.5f);
        Vector3 midPointAngularVelocity = Vector3.Lerp(leftHandAngularVelocity, rightHandAngularVelocity, 0.5f);

        if (!isGrowing && isLevitating && rb.transform.position.y < levitationCeiling) {
            // lift up
            rb.AddForce(0, 1f * rb.mass * forceMultipler, 0);
            rb.AddForce(midPointVelocity * velocityUpdateFactor);
            rb.AddTorque(midPointAngularVelocity * angularVelocityUpdateFactor);
        } else if (!isGrowing && isLevitating) {
            // keep up but no further
            rb.AddForce(0, 1f * rb.mass , 0);
            rb.AddTorque(midPointAngularVelocity * angularVelocityUpdateFactor);
        }
    }

    // Update is called once per rendered frame
    void Update() {
        // have all vertices been hit already?
        if (!isGrowing && GoalAccomplished()) {
            ShowAccomplishedLights();
            isGrowing = true;
            LTDescr tweenDescriptor = GrowCube();
            tweenDescriptor.setOnStart(() => audioSource.PlayOneShot(growAudioClip, 0.9f));
            tweenDescriptor.setOnComplete(() => {
                isGrowing = false;
                // because the cube is bigger, we have to reset the ceilings and targets
                targetScale *= growthFactor;
                levitationCeiling *= growthFactor;
                rb.mass *= growthFactor;
                ResetLights();
            });
        }
    }

    LTDescr GrowCube() {
        int tweenId = LeanTween.scale(gameObject, targetScale, 3).setEase(LeanTweenType.easeInElastic).id;
        return LeanTween.descr(tweenId);
    }

    void ShowAccomplishedLights()
    {
        foreach (var vertex in cubeVerticesWithLight)
        {
            vertex.GetComponent<Light>().color = Color.green;
            vertex.GetComponent<Light>().range = 0.5f;
        }
    }

    void ResetLights() {
        foreach (var vertex in cubeVerticesWithLight) {
            vertex.GetComponent<Light>().color = Color.white;
            vertex.GetComponent<Light>().range = 0;
        }
    }

    bool GoalAccomplished() {
        // TODO: rework as async check with delegates...
        return cubeVerticesWithLight.ToArray().All(_gameObject => _gameObject.GetComponent<Light>().range >= lightRangeTreshold);
    }
}
